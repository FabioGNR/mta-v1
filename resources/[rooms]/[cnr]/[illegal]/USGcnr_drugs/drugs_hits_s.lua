local playerActiveDrugs = {}
local TIME_PER_HIT = 60000

function takeDrug(player, drug, amount)
	if(not isElement(player) or exports.USGrooms:getPlayerRoom(player) ~= "cnr" or not drugs[drug]) then
		return false
	end
	amount = math.floor(amount)
	if(takePlayerDrugs(player, drug, amount)) then
		local activeTime = TIME_PER_HIT*amount
		if(playerActiveDrugs[player]) then
			if(playerActiveDrugs[player][drug]) then
				playerActiveDrugs[player][drug] = playerActiveDrugs[player][drug] + activeTime -- already applied
			else
				playerActiveDrugs[player][drug] = activeTime+getTickCount()
				applyDrugEffect(player, drug)
			end
		else
			playerActiveDrugs[player] = { [drug] = activeTime+getTickCount() }
			applyDrugEffect(player, drug)
		end
		triggerClientEvent(player, "USGcnr_drugs.onDrugTaken", player, drug, activeTime)
	else
		exports.USGmsg:msg(player, "You do not have enough "..drug.." to take "..amount.." hits!", 255, 0, 0)
		return false
	end
end

function decreaseActiveDrugs()
	local tick = getTickCount()
	for player, drugs in pairs(playerActiveDrugs) do
		local canDelete = true
		for drug, endTick in pairs(drugs) do
			if(tick >= endTick) then
				removeDrugEffect(player, drug)
				drugs[drug] = nil
			else
				canDelete = false -- still an active drug
			end
		end
		if(canDelete) then -- no more active drugs, remove this player
			playerActiveDrugs[player] = nil
		end
	end
end
setTimer(decreaseActiveDrugs,1000,0)

function applyDrugEffect(player, drug)
	if(drug == "Ritalin") then
		triggerClientEvent(player, "USGcnr_drugs.applyEffect", player, drug)
	elseif(drug == "Cocaine") then
		triggerClientEvent(player, "USGcnr_drugs.applyEffect", player, drug)
	elseif(drug == "Ecstacy") then
		setPedStat(player, 24, 1000)
	elseif(drug == "Heroin") then
		addEventHandler("onPlayerDamage", player, onPlayerDamage)
	elseif(drug == "Weed") then
		triggerClientEvent(player, "USGcnr_drugs.applyEffect", player, drug)
	else
		return false
	end
	return true
end

function removeDrugEffect(player, drug)
	if(drug == "Ritalin") then
		triggerClientEvent(player, "USGcnr_drugs.removeEffect", player, drug)
	elseif(drug == "Cocaine") then
		triggerClientEvent(player, "USGcnr_drugs.removeEffect", player, drug)
	elseif(drug == "Ecstacy") then
		setPedStat(player, 24, 569)
		local hp = getElementHealth(player)
		if (hp < 90) then
			setElementHealth(player, hp + 10)
		end
	elseif(drug == "Heroin") then
		removeEventHandler("onPlayerDamage", player, onPlayerDamage)
	elseif(drug == "Weed") then
		triggerClientEvent(player, "USGcnr_drugs.removeEffect", player, drug)
	else
		return false
	end
	return true
end

addEvent("USGcnr_drugs.takeDrug", true)
function onPlayerTakeDrug(drug, amount)
	takeDrug(client, drug, amount)
end
addEventHandler("USGcnr_drugs.takeDrug", root, onPlayerTakeDrug)

local heroinTimeout = {}

function onPlayerExit()
	heroinTimeout[source] = nil
	if(playerActiveDrugs[source]) then
		for drug, amount in pairs(playerActiveDrugs[source]) do
			removeDrugEffect(source, drug)
		end
	end
	playerActiveDrugs[source] = nil
end

addEventHandler("onPlayerExitRoom",root,onPlayerExit)
addEventHandler("onPlayerQuit",root,onPlayerExit)

function onPlayerDamage(att, wep, part, loss) -- apply any heroin effects
	if (playerActiveDrugs[source] and playerActiveDrugs[source].Heroin and playerActiveDrugs[source].Heroin-getTickCount() > 0 and exports.USGrooms:getPlayerRoom(source) == "cnr") then
		if(getPlayerPing(source) > 400 and heroinTimeout[source] and getTickCount()-heroinTimeout[source] < 1000) then return end -- disable for laggers
		if(getPedArmor(source) <= 0 and loss > 3) then
			local hp = getElementHealth(source)
			if (hp > 2) then
				heroinTimeout[source] = getTickCount()
				setElementHealth(source, hp + (math.floor(loss) / 10))
			end
		end
	end
end
addEventHandler("onPlayerDamage", root, onPlayerDamage)