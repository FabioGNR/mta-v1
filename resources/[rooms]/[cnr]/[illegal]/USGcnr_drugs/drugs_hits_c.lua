activeDrugs = {} -- [drug] = {start,end}

function takeDrug(drug,amount)
	local amount = math.floor(amount)
	triggerServerEvent("USGcnr_drugs.takeDrug", localPlayer, drug, amount) -- server will deal with checking amount ( safer )
end


addEvent("USGcnr_drugs.onDrugTaken", true)
function onDrugTaken(drug, activeTime)
	if(not activeDrugs[drug]) then
		activeDrugs[drug] = {getTickCount(), getTickCount()+activeTime}
	else
		activeDrugs[drug][2] = activeDrugs[drug][2]+activeTime
	end
end
addEventHandler("USGcnr_drugs.onDrugTaken", localPlayer, onDrugTaken)

addEvent("USGcnr_drugs.applyEffect", true)
function applyDrugEffect(drug)
	if(drug == "Ritalin") then
		setGameSpeed(1.1)
	elseif(drug == "Cocaine") then
		addEventHandler("onClientRender", root, renderCocaine)
	elseif(drug == "Ecstacy") then
	
	elseif(drug == "Heroin") then
	
	elseif(drug == "Weed") then
		setGravity(0.008 - 0.0038)
	else
		return false
	end
	return true
end
addEventHandler("USGcnr_drugs.applyEffect", localPlayer, applyDrugEffect)

addEvent("USGcnr_drugs.removeEffect", true)
function removeDrugEffect(drug)
	if(drug == "Ritalin") then
		setGameSpeed(1.0)
	elseif(drug == "Cocaine") then
		removeEventHandler("onClientRender", root, renderCocaine)
	elseif(drug == "Ecstacy") then
	
	elseif(drug == "Heroin") then
	
	elseif(drug == "Weed") then
		setGravity(0.008)
	else
		return false
	end
	return true
end
addEventHandler("USGcnr_drugs.removeEffect", localPlayer, removeDrugEffect)

addEventHandler("onClientResourceStop", resourceRoot,
	function ()
		if(activeDrugs) then
			for drug, endTick in pairs(activeDrugs) do
				removeDrugEffect(drug)
			end
		end
	end
)

local COCAINE_DISTANCE = 40

function renderCocaine()
	local x,y,z = getElementPosition(localPlayer)
	for i, player in ipairs(getElementsByType("player")) do
		if(exports.USGrooms:getPlayerRoom(player) == "cnr" and player ~= localPlayer and isElementOnScreen(player)) then
			local px,py,pz = getPedBonePosition(player, 8)
			local dist = getDistanceBetweenPoints2D(x,y,px,py)
			if(dist < COCAINE_DISTANCE and dist > 1) then
				local sx,sy = getScreenFromWorldPosition(px,py,pz,0)
				if(sx and sy) then
					local size = math.ceil(dist/COCAINE_DISTANCE)*20
					dxDrawRectangle(sx-math.floor(size/2),sy-math.floor(size/2),size,size,tocolor(210,0,0,115+((COCAINE_DISTANCE/dist)/COCAINE_DISTANCE)*140))
				end
			end
		end
	end
end

function cmdTakeDrug(cmd, arg1, arg2)
	if(exports.USGrooms:getPlayerRoom() ~= "cnr") then return false end
	local drug, amount
	if(tonumber(arg1) and not arg2 or arg2 == "") then
		drug = getSelectedDrug()
		amount = tonumber(arg1)
	elseif(arg1 and arg2) then
		drug = arg1
		amount = tonumber(arg2)
	end
	if(not amount or math.floor(amount) <= 0) then
		exports.USGmsg:msg("Amount must be more than 0, syntaxes: /cmd drug amount | /cmd amount",255,0,0)
		return false
	end
	amount = math.floor(amount)
	if(drug) then
		for i, drugType in ipairs(getDrugTypes()) do
			if(drugType:lower() == drug:lower()) then
				drug = drugType
				break
			end
		end
	end
	if(not drug or not drugs[drug]) then
		exports.USGmsg:msg(
			tonumber(arg1) and "You have not selected any drugs!" 
			or "This drug doesn't exist, syntaxes: /cmd drug amount | /cmd amount"
		, 255, 0, 0)
		return false
	end
	takeDrug(drug, amount)
end
addCommandHandler("takedrug", cmdTakeDrug, false, false)
addCommandHandler("takehit", cmdTakeDrug, false, false)

function renderDrugs()
	local x = 15
	local layout = getChatboxLayout()
	local y = 20+(1+layout["chat_lines"])*layout["chat_scale"][2]*15
	for drug, times in pairs(activeDrugs) do
		local progress = (getTickCount()-times[1])/(times[2]-times[1])
		if(progress >= 1) then
			activeDrugs[drug] = nil
		else
			local r,g,b = progress*255, (1-progress)*255, 0
			dxDrawRectangle(x, y, 150,20,tocolor(0,0,0,200))
			dxDrawRectangle(x+2, y+2, 146*(1-progress),16,tocolor(r,g,b,255))
			dxDrawText(drug,x,y,x+150,y+20,tocolor(255,255,255),1,"default-bold","center","center")
			y = y +25
		end
	end
end