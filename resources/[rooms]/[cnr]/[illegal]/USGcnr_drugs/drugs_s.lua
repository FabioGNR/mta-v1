--IMPORTANT STUFF--
loadstring(exports.MySQL:getQueryTool())() --load the custom mysql tool

local defaultDrugs = {}

for k,_ in pairs(drugs) do
	defaultDrugs[k] = 0
end

local listeningPlayers = {}

--END OF IMPORTANT STUFF--

function isResourceReady(name)
	return getResourceFromName(name) and getResourceState(getResourceFromName(name)) == "running"
end

addEventHandler("onResourceStart", root,
	function (res) -- init job if thisResource started, or if USGcnr_jobs (re)started
		if((source == resourceRoot and isResourceReady("USGcnr_inventory")) or res == getResourceFromName("USGcnr_inventory")) then
			for name, drug in pairs(drugs) do
				exports.USGcnr_inventory:create(drug.key, "SMALLINT UNSIGNED", 0)
			end
		end
	end
)

function onPlayerRoomExit(prevRoom)
	listeningPlayers[source] = nil
end

function onPlayerQuit()
	listeningPlayers[source] = nil	
end
addEvent("onPlayerExitRoom", true)
addEventHandler("onPlayerExitRoom",root,onPlayerRoomExit)
addEventHandler("onPlayerQuit",root,onPlayerQuit)

function getPlayerDrugs(player)
	if(isElement(player) and exports.USGrooms:getPlayerRoom(player) == "cnr") then
		local pDrugs = {}
		for name, drug in pairs(drugs) do
			pDrugs[name] = exports.USGcnr_inventory:get(player, drug.key)
		end
		return pDrugs
	end
	return false
end

function getPlayerDrugAmount(player, drugType)
	if (player) and (isElement(player)) then
		if (exports.USGaccounts:isPlayerLoggedIn(player) and exports.USGrooms:getPlayerRoom(player) == "cnr") then
			return exports.USGcnr_inventory:get(player, drugs[drugType].key) or false
		else
			return false
		end
	else
		return false
	end
end

function givePlayerDrugs(player, drug, amount)
	if(not isElement(player) or exports.USGrooms:getPlayerRoom(player) ~= "cnr") then return false end
	exports.USGcnr_inventory:add(player, drugs[drug].key, amount)
	if(listeningPlayers[player]) then
		triggerClientEvent(player, "USGcnr_drugs.recieveDrugs", player, { [drug] = exports.USGcnr_inventory:get(player, drugs[drug].key) })
	end	
	return true
end

function takePlayerDrugs(player, drug, amount)
	if(not isElement(player) or not drugs[drug]) then return false end
	exports.USGcnr_inventory:take(player, drugs[drug].key, amount)
	if(listeningPlayers[player]) then
		triggerClientEvent(player, "USGcnr_drugs.recieveDrugs", player, { [drug] = exports.USGcnr_inventory:get(player, drugs[drug].key) })
	end	
	return true
end

addEvent("USGcnr_drugs.requestDrugs", true)
function playerRequestDrugs()
	listeningPlayers[client] = true
	triggerClientEvent(client, "USGcnr_drugs.recieveDrugs", client, getPlayerDrugs(client))
end
addEventHandler("USGcnr_drugs.requestDrugs", root, playerRequestDrugs)