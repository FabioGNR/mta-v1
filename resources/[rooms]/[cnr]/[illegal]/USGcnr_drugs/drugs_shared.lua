drugs = {
	Ritalin = {id = 1580, key = "ritalin"},
--	LSD = {id = 1575},
	Cocaine = {id = 1576, key = "cocaine"},
	Ecstacy = {id = 1577, key = "ecstacy"},
	Heroin = {id = 1578, key = "heroin"},
	Weed = {id = 1579, key = "weed"}
}

local types = {}
for drug, _ in pairs(drugs) do
	table.insert(types, drug)
end

function getDrugTypes()
	return types
end