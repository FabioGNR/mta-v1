local drugsGUI = {}
function createDrugsGUI()
	drugsGUI.window = exports.USGGUI:createWindow("center","center",200,255,false,"Drugs")
	exports.USGGUI:setDefaultTextAlignment("left","center")
	drugsGUI.drugLabels = {}
	drugsGUI.drugRadios = {}
	local y = 5
	for drug, _ in pairs(drugs) do
		drugsGUI.drugLabels[drug] = exports.USGGUI:createLabel(5,y,135,30,false,drug..":",drugsGUI.window)
		drugsGUI.drugRadios[drug] = exports.USGGUI:createRadioButton(145,y,60,35,false, myDrugs and tostring(myDrugs[drug]) or "0", drugsGUI.window)
		y = y+35
	end
	drugsGUI.amount = exports.USGGUI:createEditBox(5,y+5,110,25,false,"",drugsGUI.window)
	drugsGUI.take = exports.USGGUI:createButton(130,y+5,60,25,false,"Take",drugsGUI.window)
	exports.USGGUI:setSize(drugsGUI.window, 200, y+35, false)
	addEventHandler("onUSGGUISClick", drugsGUI.take, onTakeDrug, false)
end

function updateMyDrugs(new)
	for drug, amount in pairs(new) do
		myDrugs[drug] = amount
	end
end

addEvent("USGcnr_drugs.recieveDrugs", true)
function recieveDrugs(drugs)
	if(not myDrugs) then myDrugs = drugs
	else updateMyDrugs(drugs) end
	if(isElement(drugsGUI.window)) then
		for drug, amount in pairs(drugs) do
			if(drugsGUI.drugRadios[drug]) then
				exports.USGGUI:setText(drugsGUI.drugRadios[drug], amount)
			end
		end
	end
end
addEventHandler("USGcnr_drugs.recieveDrugs", localPlayer, recieveDrugs)

function toggleDrugsGUI()
	if(not isElement(drugsGUI.window) or not exports.USGGUI:getVisible(drugsGUI.window)) then
		openDrugsGUI()
	else
		closeDrugsGUI()
	end
end
addCommandHandler("drugs", toggleDrugsGUI)
bindKey("F4","down","drugs")

function openDrugsGUI()
	if(exports.USGrooms:getPlayerRoom() ~= "cnr") then return false end
	if(not isElement(drugsGUI.window)) then
		createDrugsGUI()
	else
		exports.USGGUI:setVisible(drugsGUI.window, true)
	end
	showCursor(true)
end

function closeDrugsGUI()
	exports.USGGUI:setVisible(drugsGUI.window, false)
	showCursor(false)
end

addEventHandler("onPlayerExitRoom", localPlayer,
	function (prevRoom)
		if(prevRoom == "cnr") then
			myDrugs = nil
			activeDrugs = nil
			removeEventHandler("onClientRender", root, renderDrugs)			
			if(isElement(drugsGUI.window)) then
				closeDrugsGUI()
				destroyElement(drugsGUI.window)
			end
		end
	end
)

addEventHandler("onClientResourceStart", root,
	function (res)
		if(source == resourceRoot and getResourceFromName("USGrooms") and getResourceState(getResourceFromName("USGrooms")) == "running"
		and exports.USGRooms:getPlayerRoom() == "cnr") then
			activeDrugs = {}
			triggerServerEvent("USGcnr_drugs.requestDrugs", localPlayer) -- this will also subscribe the user to updates
			addEventHandler("onClientRender", root, renderDrugs)
			addDrugTypesToInventory()
		elseif(getResourceName(res) == "USGcnr_inventory") then
			addDrugTypesToInventory()
		end
	end
)

function isResourceReady(name)
	return getResourceFromName(name) and getResourceState(getResourceFromName(name)) == "running"
end

function addDrugTypesToInventory()
	if(isResourceReady("USGcnr_inventory")) then
		for name, drug in pairs(drugs) do
			exports.USGcnr_inventory:create(drug.key,name, ":USGcnr_drugs/"..drug.key..".png")
		end
	end
end

addEventHandler("onPlayerJoinRoom", localPlayer,
	function (room)
		if(room == "cnr") then
			addDrugTypesToInventory()
			activeDrugs = {}
			triggerServerEvent("USGcnr_drugs.requestDrugs", localPlayer) -- this will also subscribe the user to updates
			addEventHandler("onClientRender", root, renderDrugs)			
		end
	end
)

----

function getSelectedDrug()
	for drug, radio in pairs(drugsGUI.drugRadios) do
		if(exports.USGGUI:getRadioButtonState(radio)) then
			return drug
		end
	end
	return false
end

function onTakeDrug()
	local amount = tonumber(exports.USGGUI:getText(drugsGUI.amount))
	if(not amount or amount <= 0) then
		exports.USGmsg:msg("Invalid number, must be more than 0!", 255, 0, 0)
		return
	end
	local drug = getSelectedDrug()
	if(not drug) then
		exports.USGmsg:msg("You must select a drug!", 255, 0, 0)
		return		
	end
	takeDrug(drug, amount)
end