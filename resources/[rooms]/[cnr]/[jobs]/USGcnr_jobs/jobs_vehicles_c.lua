local jobSpawners = {}
local currentMarkerVehRotation

local vehiclePickerGUI = {}

function loadJobVehicles(jobID)
	if(not jobs[jobID]) then return end
	clearJobVehicles(jobID) -- clean up any previous markers

	local markers = jobs[jobID].vehicles
	
	if(markers) then
		local vehicleSetMarkers = {}
		local vehicleSetsDone = {}	
		
		for _, marker in ipairs(markers) do
			if(not vehicleSetMarkers[marker.vehicles]) then
				vehicleSetMarkers[marker.vehicles] = {marker}
			else
				table.insert(vehicleSetMarkers[marker.vehicles], marker)
			end
		end
		local i = 0
		for vehicles, markers in pairs(vehicleSetMarkers) do
			i=i+1
			exports.USGcnr_vehiclespawners:addSpawner(markers, vehicles, {functionName = "canPlayerUseSpawner", name="job:"..jobID..":"..i})
		end
		jobSpawners[jobID] = i
	end
end

function clearJobVehicles(jobID)
	if(jobSpawners[jobID]) then
		for i=1, jobSpawners[jobID] do
			exports.USGcnr_vehiclespawners:removeSpawner("job:"..jobID..":"..i)
		end
		jobSpawners[jobID] = nil
	end
end

addEventHandler("onPlayerExitRoom", localPlayer,
	function (prev)
		if(prev == "cnr") then
			for job, _ in pairs(jobSpawners) do
				clearJobVehicles(job)
			end
		end
	end
)

addEventHandler("onClientResourceStart", root,
	function (res)
		if(res == getResourceFromName("USGcnr_vehiclespawners")) then
			for jobID, _ in pairs(jobSpawners) do -- reload all spawners
				loadJobVehicles(jobID)
			end
		end
	end
)