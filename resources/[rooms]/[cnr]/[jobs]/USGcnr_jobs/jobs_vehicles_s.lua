function canPlayerUseSpawner(player, spawnerName)
	local parts = split(spawnerName, ":")
	return parts[2] and getPlayerJob(player) == parts[2] -- match jobname ( from spawnerName format "job:<jobid>:id" ) with player's job
end

addEventHandler("onPlayerChangeJob", root, 
	function (jobID) 
		local veh = exports.USGcnr_vehiclespawners:getPlayerSpawnedVehicle(source)
		if(isElement(veh)) then
			local name = exports.USGcnr_vehiclespawners:getPlayerVehicleSpawner(source)
			local parts = split(name, ":")
			if(parts[1] == "job" and parts[2] and jobID ~= parts[2]) then -- switched to a different job and current vehicle-spawner is a job-spawner
				exports.USGcnr_vehiclespawners:removePlayerVehicle(source)
			end
		end
	end
)