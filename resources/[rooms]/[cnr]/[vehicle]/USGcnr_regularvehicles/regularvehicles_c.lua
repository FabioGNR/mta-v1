local vehicles = {
	{model=401},
	{model=402},
	{model=412},
	{model=418},
	{model=426},
	{model=496},
	{model=510},
}

local markers = {
	r = 255, g = 255, b = 255, a = 175,
	{x = -2661.86328125, y = 630.97265625, z = 14.453125, vehRotation = 180},
	{x = -1503.52734375, y = 2525.654296875, z = 55.6875, vehRotation = 0},
	{x = -312.9111328125, y = 1058.0537109375, z = 19.7421875, vehRotation = 0},
	{x = 1626.203125, y = 1822.5732421875, z = 10.8203125, vehRotation = 0},
	{x = 1236.443359375, y = 342.66015625, z = 19.5546875, vehRotation = 245},
	{x = 1187.701171875, y = -1315.19140625, z = 13.564745903015, vehRotation = 270},
	{x = 2001.888671875, y = -1435.4482421875, z = 14.240371704102, vehRotation = 180},
	{x = -2189.53125, y = -2295.0712890625, z = 30.625, vehRotation = 315},
	{x = -1613.708984375, y = 720.86328125, z = 13.448682785034, vehRotation = 0},
	{x = 633.740234375, y = -561.4873046875, z = 16.3359375, vehRotation = 270},
	{x = 1536.0703125, y = -1672.197265625, z = 13.3828125, vehRotation = 182.59829711914},
	-- ORIGINALLY CRIM SPAWNERS
	{ x = -1895.7265625, y = -550.3994140625, z = 24.5937,vehRotation=90},
	{ x = -2491.18359375, y = 1214.7353515625, z = 37.421875,vehRotation=150},
	{ x = -2491.18359375, y = 1214.7353515625, z = 37.421875,vehRotation=150},
	{ x = 1968.44921875, y = 2199.1669921875, z = 10.8203125,vehRotation=270},
	{ x = 1822.7255859375, y = 808.3125, z = 10.8203125,vehRotation=305},
	{ x = 1307.0810546875, y = -1067.533203125, z = 29.225914001465,vehRotation=0},
	{ x = 389.1025390625, y = -1725.119140625, z = 7.9620251655579,vehRotation=90},
	-- REGULAR SPAWNERS
}

addEvent("onPlayerJoinRoom", true)
addEventHandler("onPlayerJoinRoom", localPlayer,
	function (room)
		if(room == "cnr") then
			exports.USGcnr_vehiclespawners:addSpawner(markers, vehicles, {name="regular"})
		end
	end
)

addEventHandler("onClientResourceStart", root,
	function (res)
		if((resource == res or res == getResourceFromName("USGcnr_vehiclespawners")) and getResourceFromName("USGrooms")
		 and getResourceState(getResourceFromName("USGrooms")) == "running" and exports.USGrooms:getPlayerRoom() == "cnr") then
			exports.USGcnr_vehiclespawners:addSpawner(markers, vehicles, {name="regular"})
		end
	end
)