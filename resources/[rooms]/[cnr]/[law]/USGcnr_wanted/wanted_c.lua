function getPlayerWantedLevel(player)
	local thePlayer = player ~= nil and player or localPlayer
	if(not isElement(thePlayer) or not exports.USGrooms:getPlayerRoom(thePlayer) == "cnr") then
		error("Not a player, syntax: getPlayerWantedLevel(player) | getPlayerWantedLevel() ")
	end
	return getElementData(thePlayer, "wantedLevel")
end

-- events for becoming wanted
addEventHandler("onClientVehicleDamage", root,
	function (attacker)
		if(exports.USGrooms:getPlayerRoom() ~= "cnr") then return end
		if(attacker == localPlayer) then
			local hasOccupant = false
			local occupants = getVehicleOccupants(source)
			local isCop = exports.USGcnr_jobs:getPlayerJobType(localPlayer) == "police"
			for seat, occupant in pairs(occupants) do
				if(occupant == localPlayer) then
					return false
				end
				if(isElement(occupant) and getElementType(occupant) == "player"
				and (not isCop or exports.USGcnr_wanted:getPlayerWantedLevel(occupant) == 0)) then
					hasOccupant = true
				end
			end
			if(hasOccupant) then
				triggerServerEvent("USGcnr_wanted.reportVehicleDamage", localPlayer)
			end
		end
	end
)