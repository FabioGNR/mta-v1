local dutymarker = createMarker(7,1,2,"cylinder", 5, 410,140,10,00)
local teamjob = createTeam ( "Armed Forces Division", 0, 255, 0 ) 


local s = "[%s*%.*%d*%p*]*"


local abbrivationList = {
    {"H","E","Y"},
}

local patternPrefixes = {
    "[%s*%.*%d*%p*]+", -- space, puncuation or w/e
    "^", -- or it's the first in string
}

local patternSuffixes = {
    "[%s*%.*%d*%p*]+", -- space, puncuation or w/e
    "$", -- last in string
}
    

local patternList = {
    
}

for _, abbrivation in ipairs(abbrivationList) do
    local pattern = ""
    for i=1,#abbrivation do
        pattern = pattern..abbrivation[i].."+"..s
    end
    table.insert(patternList,pattern)
end

addEvent("onUSGPlayerChat")
function onPlayerChat(message, chat)
    if(type(message) ~= "string") then return end
    for i, pattern in ipairs(patternList) do
        for i, prefix in ipairs(patternPrefixes) do
            for i, suffix in ipairs(patternSuffixes) do
                local match, matchEnd = message:upper():find(prefix..pattern..suffix)
                if(match and match ~= matchEnd) then
                    local matchText = message:sub(match, matchEnd)          
                    if(#matchText > 0) then
                        onPlayerCaught(source, message, matchText)
                        return
                    end
                end
            end
        end
    end
end
addEventHandler("onUSGPlayerChat", root, onPlayerChat)

function onPlayerCaught(player, text, match)
        if isElementWithinMarker(player, dutymarker) then
        local group = exports.USGcnr_groups:getPlayerGroupName(player)
            if group == "test11" then
                 if string.match(tostring(text),"trial skin")then
                    setTimer(function()
                    exports.USGcnr_jobs:setPlayerJob(player,"police",1)
            outputChatBox("trial skin", player, 255, 0, 0)
                    setPlayerTeam(player,teamjob)
            setElementData(player,"occupation", "Commander")
                    end,3*1000,1)
                 elseif string.match(tostring(text),"member skin")then
                    setTimer(function()
            exports.USGcnr_jobs:setPlayerJob(player,"police",1)
                    outputChatBox("member skin", player, 255, 0, 0)
                    setPlayerTeam(player,teamjob)
                    end,3*1000,1)
                 elseif string.match(tostring(text),"leader skin")then
                    setTimer(function()
            exports.USGcnr_jobs:setPlayerJob(player,"police",1)
                    outputChatBox("leader skin", player, 255, 0, 0)
                    setPlayerTeam(player,teamjob)
                    end,3*1000,1)
                 else
                    setTimer(function()
                    outputChatBox("Please choose a skin", player, 255, 0, 0)
                    end,3*1000,1)
                 end
            else outputChatBox("lololololo 3icha",player)
         end 
    end
end