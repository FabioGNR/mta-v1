local getAccountID = function (...) return exports.server:getPlayerAccountID(...) end

function onBankHit()
	local acc = exports.USGaccounts:getPlayerAccountID(source)
	local bData = exports.mysql:singleQuery("SELECT * FROM banking WHERE userid=? LIMIT 1",acc)
	if not (bData) then
		exports.USGmsg:msg(source,"Seems you don't have a bank account, we'll make one for you!",255,255,0,3000)
		exports.mysql:execute("INSERT INTO banking SET userid=?,serial=?",acc,getPlayerSerial(source))
	elseif (bData.serial == getPlayerSerial(source)) then
		triggerClientEvent(source,"showBankingGui",source,bData.balance)
		triggerClientEvent(source,"updateBalanceLabel",source,bData.balance)
	else
		triggerClientEvent(source,"requestAccDetails",source)
	end
end
addEvent("onBankHit",true)
addEventHandler("onBankHit",root,onBankHit)

function bank_withdrawMoney(amount)
	if not (type(amount) == "number") then
		exports.USGmsg:msg(source,"Numbers only!",255,0,0,2000)
		return false
	end
	
	local acc = exports.USGaccounts:getPlayerAccountID(source)
	local bData = exports.mysql:singleQuery("SELECT * FROM banking WHERE userid=? LIMIT 1",acc)
	
	local give = amount --amount to give to player if successful
	local newBalance = (bData.balance - value)
	
	if (tonumber(amount) > tonumber(bData.balance)) or (bData.balance == 0) then
		exports.USGmsg:msg(source,"You don't have enough money in your bank account!",255,0,0,3000)
	else
		if (exports.mysql:execute("UPDATE banking SET balance=?,serial=? WHERE userid=?",newBalance,getPlayerSerial(source),acc)) then
			givePlayerMoney(source,give)
			exports.system:log("money",getPlayerName(source).." withdrawn "..amount.." from his bank. (Balance: "..bData.balance..", New: "..bData.balance-amount..")",source)
			triggerClientEvent(source,"updateBalanceLabel",source,newBalance)
		else
			exports.USGmsg:msg(source,"An error occured while withdrawing your money. This problem has been logged.",255,0,0,3000)
			exports.system:logError("Failed to update "..getPlayerName(source).."'s bank balance.")
		end
	end
end
addEvent("bank:withdraw",true)
addEventHandler("bank:withdraw",root,bank_withdrawMoney)

function bank_depositMoney(amount)
	if not (type(amount) == "number") then
		exports.USGmsg:msg(source,"Numbers only!",255,0,0,2000)
		return false
	end
	
	local acc = exports.USGaccounts:getPlayerAccountID(source)
	local bData = exports.mysql:singleQuery("SELECT * FROM banking WHERE userid=? LIMIT 1",acc)
	
	local newBalance = (bData.balance + amount)
	
	if (tonumber(amount) > tonumber(getPlayerMoney(source))) or (getPlayerMoney(source) == 0) then
		exports.USGmsg:msg(source,"You have insufficient funds to deposit into your bank account.",255,0,0,3000)
	else
		if (exports.mysql:execute("UPDATE banking SET balance=?,serial=? WHERE userid=?",newBalance,getPlayerSerial(source),acc)) then
			takePlayerMoney(source,amount)
			exports.system:log("money",getPlayerName(source).." deposited "..amount.." to his bank. (Balance: "..bData.balance..", New: "..bData.balance+amount..")",source)
			triggerClientEvent(source,"updateBalanceLabel",source,newBalance)
		else
			exports.USGmsg:msg(source,"An error occured while depositing your money. This problem has been logged.",255,0,0,3000)
			exports.system:logError("Failed to update "..getPlayerName(source).."'s bank balance!")
		end
	end
end
addEvent("bank:deposit",true)
addEventHandler("bank:deposit",root,bank_depositMoney)

