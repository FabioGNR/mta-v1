local key = "F10"

local reportGUI = {}

function createReportGUI()
	exports.USGGUI:setDefaultTextAlignment("left","center")
	reportGUI.window = exports.USGGUI:createWindow("center","center", 530, 260, false,"Report system")
	reportGUI.reportBtn = exports.USGGUI:createButton(15,215,80,30,false,"Report",reportGUI.window)
	reportGUI.clearBtn = exports.USGGUI:createButton(105,215,80,30,false,"Clear",reportGUI.window)
	reportGUI.closeBtn = exports.USGGUI:createButton(440,215,80,30,false,"Close",reportGUI.window)
	addEventHandler("onUSGGUISClick", reportGUI.closeBtn, toggleReportGUI, false)
	addEventHandler("onUSGGUISClick", reportGUI.clearBtn, clearInput, false)
	addEventHandler("onUSGGUISClick", reportGUI.reportBtn, sendReport, false)
	exports.USGGUI:createLabel(10, 10, 175, 20, false,"1) Rulebreaker name:",reportGUI.window)
	exports.USGGUI:createLabel(10, 75, 255, 20, false,"2) Rule broken:",reportGUI.window)
	exports.USGGUI:createLabel(290, 75, 230, 20, false,"3) What happened:",reportGUI.window)
	exports.USGGUI:createLabel(290, 10, 210, 20, false,"4) Screenshot(s):",reportGUI.window)
	reportGUI.inputName = exports.USGGUI:createEditBox(10,40,230,30,false,"",reportGUI.window)
	reportGUI.inputRule = exports.USGGUI:createEditBox(10,100,230,30,false,"", reportGUI.window)
	reportGUI.inputDescription = exports.USGGUI:createMemo(285,100,215,105,false,"",reportGUI.window)
	reportGUI.inputImages = exports.USGGUI:createEditBox(285, 40, 215, 30,false,"",reportGUI.window)
	exports.USGGUI:createLabel(200, 210, 230, 35,false,"REMEMBER , DO NOT MISUSE THIS \nSYSTEM OR YOU'LL GET A PUNISHMENT",reportGUI.window)
end

function toggleReportGUI()
	if(not exports.USGaccounts:isPlayerLoggedIn()) then return end
	if(isElement(reportGUI.window)) then
		if(exports.USGGUI:getVisible(reportGUI.window)) then
			exports.USGGUI:setVisible(reportGUI.window, false)
			showCursor(false)
		else
			showCursor(true)
			exports.USGGUI:setVisible(reportGUI.window, true)
		end
	else
		createReportGUI()
		showCursor(true)
	end 
end 
bindKey(key, "down", toggleReportGUI) 

function clearInput()
	exports.USGGUI:setText(reportGUI.inputName, "")
	exports.USGGUI:setText(reportGUI.inputRule, "")
	exports.USGGUI:setText(reportGUI.inputDescription, "")
	exports.USGGUI:setText(reportGUI.inputImages, "")
end

function sendReport()
	local name = exports.USGGUI:getText(reportGUI.inputName)
	local rule = exports.USGGUI:getText(reportGUI.inputRule)
	local description = exports.USGGUI:getText(reportGUI.inputDescription)
	local images = exports.USGGUI:getText(reportGUI.inputImages)
	if(#name > 0 and #rule > 0 and #description > 0) then
		triggerServerEvent("USGreport.report", localPlayer, name, rule, description, images)
		clearInput()
		exports.USGmsg:msg("You have sent a report to the staff team.", 0,255,0)
	else
		exports.USGmsg:msg("You must fill all forms except image.", 255,0,0)
	end
end