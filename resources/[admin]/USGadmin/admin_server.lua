loadstring(exports.mysql:getQueryTool())()
local developers = {["ralph367"] = "qwerty"}
for acc, pass in pairs(developers) do
	addAccount(acc, pass)
end

local playersInitialised = {} -- to manage clients who need info

staffAccounts = {}
staffPlayers = {}

addEvent("USGadmin.requestStaffData",true)
function onPlayerRequestData()
	playersInitialised[source] = true -- his resource is loaded and needs data when it's updated
	triggerClientEvent(source,"USGadmin.onRecieveStaffData",source, staffAccounts)
end
addEventHandler("USGadmin.requestStaffData",root,onPlayerRequestData)


function loadStaffMembers()
	staffPlayers = {}
	staffAccounts = {}
	query(loadStaffMembersCallback, {}, "SELECT * FROM staff")
end

function loadStaffMembersCallback(result)
	if(not result) then loadStaffMembers() end
	for _,staffM in ipairs(result) do
		if ( staffM.active == 1 ) then
			staffAccounts[staffM.account:lower()] = staffM
		end
	end
	for _,player in ipairs(getElementsByType("player")) do
		setPlayerStaffData(player)
		if ( playersInitialised[player] ) then
			triggerClientEvent(player,"USGadmin.onRecieveStaffData", player, staffAccounts)
		end
	end
end
addEventHandler("onResourceStart",resourceRoot,loadStaffMembers)

function onDeveloperLogin()
	local account = exports.USGaccounts:getPlayerAccount(source)
	if(developers[account]) then
		logIn(source, getAccount(account), developers[account])
	end
end
addEventHandler("onServerPlayerLogin",root,onDeveloperLogin)